  import numpy as np
  import pandas as pd
  import matplotlib.pyplot as plt
  plt.style.use('ggplot')
  
  df1 = pd.read_csv('hmm2statestg01c.csv')
  df2 = pd.read_csv('enso_idxLi.csv')
  
  date = np.arange('1712', '1996', dtype='datetime64[Y]')
  s1 = df1['S1'].to_numpy()
  s2 = df1['S2'].to_numpy()
  enso = df2['idx'].to_numpy()
  
  fig = plt.figure(figsize=(20, 8));
  ax = fig.add_subplot(111);
  ax.plot(date, enso, 'black', alpha=1, linewidth=2);
  ax.fill_between(date, 0., enso, enso > 0, color = '#54e1e3');
  ax.fill_between(date, 0., enso, enso < 0, color = '#b81402');
  plt.xlabel('time (year)', fontsize=15);
  plt.ylabel('reconstructed ENSO index (Li et al., 2011)', fontsize=15);
  plt.tight_layout()
  plt.savefig('ensoLiEtal11.png')
  
  plt.figure(figsize=(20, 8));
  plt.plot(date, s1, '--', label='Regime 1');
  plt.plot(date, s2, '--', label='Regime 2');
  plt.xlabel('time (year)', fontsize=15);
  plt.ylabel('probability', fontsize=15);
  plt.legend()
  plt.tight_layout()
  plt.savefig('regimePosterior.png')